#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "points.h"

int main(int argc, char **argv) {
	if (argc != 3) {
		printf("Bad number of arguments\n");
		printf("Usage:\n%s type_of_execution number_of_points\n",
			argv[0]);
		printf("Type of execution: 0 - sequential, 1 - parallel \n");
		exit(-1);
	}

	srand(time(0));

	int type_of_execution = atoi(argv[1]);
	int array_len = atoi(argv[2]);
	int *array = initialize_array(array_len);

	struct timeval time_start, time_end;

	gettimeofday(&time_start, NULL);

	float average; 

	if (type_of_execution == 0) {
		average = calculate_average(array, array_len);
	} else if (type_of_execution == 1) {
		printf("Max number of threads - %d\n", omp_get_max_threads());
		average = calculate_average_parallel(array, array_len);
	} else {
		printf("Unrecognized type_of_execution %d",
			type_of_execution);
	}

	gettimeofday(&time_end, NULL);

	int time = (time_end.tv_sec - time_start.tv_sec) * 1000 +
                   (time_end.tv_usec - time_start.tv_usec) / 1000;

	//print_array(array, array_len);
	printf("Average equals %f for %d points calculated in time %d ms\n", 
		average, array_len, time);

	return 0;
}
