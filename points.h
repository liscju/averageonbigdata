#ifndef __POINTS_H__
#define __POINTS_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int* initialize_array(int array_len);

float calculate_average(int *array, int array_len);

float calculate_average_parallel(int *array, int array_len);

void print_array(int *array, int array_len);

#endif // __POINTS_H__
