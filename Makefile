all: main.o

clean:
	rm -f *.o

main.o: main.c points.o
	gcc -fopenmp main.c -o main.o points.o

points.o: points.h points.c
	gcc -fopenmp points.c -c
