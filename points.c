#include "points.h"

int* initialize_array(int array_len) {
	int i;
	int *array = calloc(array_len, sizeof(int));		
	for (i=0;i<array_len;i++) {
		array[i] = rand() % 10;
	}
	return array;
}

float calculate_average(int *array, int array_len) {
	int i;
	long long int sum = 0;

	for (i=0;i < array_len;i++) {
		sum += array[i];
	}

	float average = sum / (float)array_len;

	return average;
}

float calculate_average_parallel(int *array, int array_len) {
	int i;
	long long int sum = 0;

#pragma omp parallel for reduction (+:sum)
	for (i=0;i < array_len;i++) {
		sum += array[i];
	}

	float average = sum / (float)array_len;

	return average;
}

void print_array(int *array, int array_len) {
	int i;

	printf("Points = {");
	for (i=0;i<array_len;i++) {
		printf("%d,", array[i]);
	}
	printf("}\n");
}
